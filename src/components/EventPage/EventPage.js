import React from 'react';
import './EventPage.css';
import FavouriteLabel from '../FavouriteLabel';
import Helmet from "react-helmet";
    
const EventPage = (props) => {
    const event = props.events.find(event=>`/${event.url}` === props.match.url);
    props.setEventPage(true);

    const eventCardHAndler = () => {
        props.getFavouriteEvents(event.id);
    }

    return(
        <div style={{'backgroundImage': `url(./img/events/${event.id}-full.png)`}} className="event-page-bg">
            <Helmet title={`${event.title} - GOGOGO.com`}/>
            <div className="container event-page-container">
                <h1 className="h1-event">{event.title}</h1>
                <div className="event-brif">{event.brif}</div>
                <div className="event-info mb-2"><span className="text-capitalize">{event.categoryAlias}</span> в Санкт-Петербурге | {event.date} | {event.location} </div>
                <div className="event-controls">
                    <div>
                        <button className="btn btn-solid btn-xl mr-1">Купить билет</button>
                    </div>                    
                    <FavouriteLabel eventCardHAndler={eventCardHAndler} isFavorites={props.favouriteEvents.some(ev=>ev.id === event.id)}  />
                </div>
            </div>
            <div className="event-page-bg-overlay"></div>
        </div>
    )
}

export default EventPage;