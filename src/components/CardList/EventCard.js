import React from 'react';
import { Link } from 'react-router-dom';
import FavouriteLabel from '../FavouriteLabel';
const EventCard = ({event,isFavorites,getFavouriteEvents}) => {
    
    const eventCardHAndler = () => {
        getFavouriteEvents(event.id);
    }

    
        
    return(
        <div className="col-sm-12 col-md-6 col-lg-4">
            <div className="event-card">
                <div className="event-card-img-cont">
                    <div className="event-card-img mb-1" style={{'backgroundImage':`url(./img/events/${event.id}-thumb.png)`}}></div>
                    <div className="event-card-img-overlay"></div>  
                    <div className="event-card-legal">{event.categoryAlias} {event.rating}<div className="event-card-legal-soft">от {event.price} руб.</div></div>
                    <FavouriteLabel eventCardHAndler={eventCardHAndler} isFavorites={isFavorites}/>             
                </div>                    
                <div className="event-card-cont">
                    <div className="card-title">{event.title}</div>
                    <div className="card-extra mb-1">{event.location} {event.date}</div>
                    <div className="card-brif mb-2">{event.brif} </div>
                    <div className="flex-pusher"></div>
                    <div className="df-sb-c">                        
                        <Link event={event} to={`${event.url}`}><button className="btn btn-solid btn-main">Подробнее</button></Link>
                        <button className="btn btn-so btn-main">Купить билет</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default EventCard;