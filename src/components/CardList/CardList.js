import React from 'react';
import EventCard from './EventCard';
import Doodle from '../Doodle';
import './CardList.css';
const CardList = (props) => {
    const events = props.events.map((event,i)=>
        // 
        i===6
        ? <div key={event.id}  className="event-card-big col-12">{event.title}</div>
        : <EventCard 
            isFavorites={props.favouriteEvents.some(ev=>ev.id === event.id)} 
            getFavouriteEvents={props.getFavouriteEvents} key={event.id} 
            event={event}
        />
    )

    return(
        <div className="card-list row">
            {props.events.length !== 0 ? events : <div className="col"><Doodle doodle={props.doodle}/></div>}
        </div>
    )
}

export default CardList;