import React, { useState, useEffect } from 'react';
import Header from '../header';
import Catalog from '../Catalog';
import EventPage from '../EventPage';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import '../../responsive.css';
import '../../main.css';
import { cats, onPageNav } from '../../data';
import { filterEvents } from '../helpers';
import { Link } from 'react-router-dom';



const App = () => {
    const [fetchedEvents, setFetchedEvents] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [autoComplete, setAutoComplete] = useState('');
    const [newEvents, setNewEvents] = useState([]);
    const [activeCategory, setActiveCategory] = useState(4);
    const [activePriceFilter, setActivePriceFilter] = useState(0);
    const [eventPage, setEventPage] = useState(false);
    const [favouriteEvents, setFavouriteEvents] = useState([]);

    const getCategories = () => {
        return cats;
    }

    useEffect(() => {
        const getEvents = async () => {
            setIsLoaded(false);
            const response = await fetch('./events.json');
            const data = await response.json();
            setFetchedEvents(data);
            setIsLoaded(true);           
        }
        getEvents();   
    }, [])

    useEffect(()=>{
        const getFavourites =  () => {
            if (isLoaded){
                const favIds = localStorage.getItem('session') ? localStorage.getItem('session').split(',') : '';
                const newFavs = favIds ? fetchedEvents.filter(event => favIds.some(favId => parseInt(favId) === event.id)) : [];
                setFavouriteEvents(newFavs);
            }            
        }
        getFavourites();
    },[isLoaded,fetchedEvents]);
    
    const clearAutoComplete = () => {
        setAutoComplete('');
    }
    const searchInputHandler = (e) => {
        setAutoComplete(e.target.value);
        setNewEvents(filterEvents(fetchedEvents, e.target.value));
    };
    const getActivePriceFilter = (priceFilter) => {
        setActivePriceFilter(priceFilter);
    }
    const getActiveCategoryId = (categoryId) => {
        setActiveCategory(categoryId);
    }

    const getFavouriteEvents = (eventId) => {
        if (favouriteEvents.some(event => event.id === eventId)) {
            const idx = favouriteEvents.findIndex(event => event.id === eventId);
            const newFavs = [...favouriteEvents]
            newFavs.splice(idx, 1);
            setFavouriteEvents(newFavs);
            localStorage.setItem('session', newFavs.map(fav => fav.id).toString());
        } else {
            const newFavs = [...favouriteEvents, fetchedEvents.find(event => event.id === eventId)];
            setFavouriteEvents(newFavs);
            localStorage.setItem('session', newFavs.map(fav => fav.id).toString());
        }
    }

    const titleCategory = getCategories().find(cat => cat.id === activeCategory).title;

    const commonProps = {
        setEventPage,
        titleCategory,
        getFavouriteEvents,
        favouriteEvents,
        onPageNav,
        activePriceFilter,
        getActivePriceFilter,
        activeCategory: getCategories().find(cat => cat.id === activeCategory)
    }

    return (

        <>
            {isLoaded
                ?
                <Router>
                    <Header
                        eventPage={eventPage}
                        activeCategory={activeCategory}
                        subNav={getCategories()}
                        autoCompleteResults={newEvents}
                        searchHandler={searchInputHandler}
                        search={autoComplete}
                        getActiveCategoryId={getActiveCategoryId}
                        favouriteEvents={favouriteEvents}
                        clearAutoComplete={clearAutoComplete}
                    />
                    <Switch>
                        <Route path="/" exact render={(props) => {
                            return (
                                <Catalog {...props}
                                    commonProps={commonProps}
                                    doodle={{ title: "Категория пуста, выберите другую" }}
                                    events={fetchedEvents}
                                    title="События в Санкт-Петербурге"
                                />
                            )
                        }} />

                        <Route path="/favourites" render={(props) => {
                            return (
                                <Catalog {...props}
                                    commonProps={commonProps}
                                    events={favouriteEvents}
                                    doodle={{ title: "Избранных событий нет", extra: <Link to="/">Вернуться в каталог</Link> }}
                                    title="Избранные события"
                                />
                            )

                        }} />

                        <Route path="/:url" render={(props) => {
                            return (
                                <EventPage {...props}
                                    events={fetchedEvents}
                                    getFavouriteEvents={getFavouriteEvents}
                                    favouriteEvents={favouriteEvents}
                                    setEventPage={setEventPage}
                                />
                            )
                        }} />
                    </Switch>
                </Router>
                :
                <div className="container">
                    <h2>Сорян, данных нет :(</h2>
                    <div>
                        <video autoPlay muted playsInline loop width="100%" height="auto">
                            <source type="video/mp4"src="./img/common/nodata.mp4"></source>
                        </video>
                    </div> 
                </div>
            }
        </>
    )
}
export default App;