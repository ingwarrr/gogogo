const filterEvents = (eventsList,value) => {
    const flteredList = [...eventsList]
    return flteredList.filter(event => {
        for (let option in event){
            if (typeof event[option] === 'string' && event[option].toLowerCase().includes(value.toLowerCase())){                   
                return event;                  
            } 
        }
        return false 
    });
}


export {filterEvents}