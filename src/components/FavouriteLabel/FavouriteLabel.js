import React from 'react';
import './FavouriteLabel.css';

const FavouriteLabel = (props) =>{
    return(
        props.isFavorites ? 
        <div onClick={props.eventCardHAndler} className="event-card-favourites-delete" title="убрать из  избранного">
            <img alt="убрать из  избранного" src="./img/common/favourites-delete.png" />
        </div>
        :
        <div onClick={props.eventCardHAndler} className="event-card-favourites" title="добавить в избранное">
            <img alt="добавить в избранное" src="./img/common/favourites.png" />
        </div>  
    )
    
}
export default FavouriteLabel;
