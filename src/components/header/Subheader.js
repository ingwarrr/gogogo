import React from 'react';

const Subheader = (props) => {
    const subNavs = props.subNav.map(subNav=>
        <li onClick={()=>props.getActiveCategoryId(subNav.id)} 
            key={subNav.id} 
            className={subNav.id === props.activeCategory ? 'subheader-nav-link active' : 'subheader-nav-link'}
            >
                {subNav.title}
        </li>
    )
    return(
        <div className="row">
            <div className="offset-lg-2 col-lg-8 col-md-12">
                <ul className="subheader">{subNavs}</ul>
            </div>
        </div>
        
    )
}

export default Subheader;