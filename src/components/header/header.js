import React from 'react';
import './header.css';
import { Link } from 'react-router-dom';

import AutoCompleteList from './AutoCompleteList';
import Subheader from './Subheader';

const Header = ({favouriteEvents,
                search,
                searchHandler,
                autoCompleteResults,
                clearAutoComplete,
                eventPage,
                subNav,
                getActiveCategoryId,
                activeCategory}) => {
    const favouritesCountClassName = favouriteEvents.length > 0 ? 'favourites-count active' : 'favourites-count';
    return(        
        <header className="header ">
            <div className="header-nav-container">
                <div className="container header-nav">
                    <Link to="/" className="header-brand mr-2">GOGOGO</Link>
                    <Link className="header-nav-link active mr-auto" to='/favourites'>
                        Избранное <span className={favouritesCountClassName}>{favouriteEvents.length}</span>
                    </Link>
                    <label className="header-nav-search-label" >
                        <input value={search} 
                            onChange={searchHandler}
                            type="text" 
                            className="header-nav-search" 
                            placeholder="поиск"
                        />                    
                        <AutoCompleteList 
                            search={search} 
                            autoCompleteResults={autoCompleteResults}
                            clearAutoComplete={clearAutoComplete}
                        /> 
                    </label>
                </div>
            </div>
            {!eventPage ?
                <nav className="container subheader-container">
                    
                    <Subheader 
                        subNav={subNav} 
                        getActiveCategoryId={getActiveCategoryId}
                        activeCategory={activeCategory}/>
                
                </nav>
             : null    
            }
        </header>        
    )
};

export default Header;