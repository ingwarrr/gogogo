import React from 'react';
import { Link } from 'react-router-dom';

const AutoCompleteList = (props) => {
    const listHandler = () =>{
        props.clearAutoComplete()
    }

    let list = [];

    if(props.autoCompleteResults && props.autoCompleteResults.length > 0 && props.search.length>0){
        list = props.autoCompleteResults.map(event=>

            <li onClick={listHandler} key={event.id} className="header-nav-search-list-item">
                <Link  to={`${event.url}`}>
                    <div className="card-title">{event.title}</div>
                    <div className="card-extra">{event.location} {event.date}</div>
                </Link>
            </li>
        )
    }
    return(
        <ul  className="header-nav-search-list">
            {list}
        </ul>
    )
}
export default AutoCompleteList