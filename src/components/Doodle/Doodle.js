import React from 'react';

const Doodle = (props) => {
    return(
        <div>
            <h2>{props.doodle.title}</h2>
            {props.doodle.extra || null}
        </div>        
        
    );
}
export default Doodle;
