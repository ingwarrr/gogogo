import React from 'react';
import CardList from '../CardList';
import './Catalog.css';
import Helmet from "react-helmet";

const Catalog = ({commonProps,events,title,doodle}) => {
    commonProps.setEventPage(false);
    const onPageNav = commonProps.onPageNav.map(navItem=>  
        <li className={commonProps.activePriceFilter === navItem.id ? 'onpage-nav-link active' : 'onpage-nav-link'} 
            key={navItem.id}
            onClick={()=>commonProps.getActivePriceFilter(navItem.id)}
        >
            {navItem.title}
        </li>
    )
        
    const filterEventsByCategory = (events,filterCase) =>{
        if(filterCase === 4){
            return events;
        }
        const newEvents = [...events].filter(event=>event.categoryId === filterCase);
        return newEvents;              
    }

    const sortEventsByPrice = (events,filterCase) => {
        switch(filterCase) {
            case 0 : 
                return [...events].sort((a,b)=>a.price - b.price);              
            case 1 :
                return [...events].sort((a,b)=>b.price - a.price);                
            default:
                return [...events].sort((a,b)=>a.id - b.id); 
        }
    }
    const sortedEvents = sortEventsByPrice(filterEventsByCategory(events, commonProps.activeCategory.id),commonProps.activePriceFilter);

    return (
        
        <section className="catalog-container container mt-2">
            <Helmet title={`${title} - GOGOGO.com`}/>
            <h1 className="h1-main">{title} - {commonProps.titleCategory}</h1>
            
            <ul  className="onpage-nav">
                {onPageNav}
            </ul>
            <div className="row">
                <div className="col-md-12 col-lg-10">
                    <CardList 
                        doodle={doodle}
                        favouriteEvents={commonProps.favouriteEvents} 
                        getFavouriteEvents={commonProps.getFavouriteEvents} 
                        events={sortedEvents}
                        activeCategory={commonProps.activeCategory}
                    />
                </div>
                <div className="d-none d-lg-block  col-lg-2">
                   <img alt="Временный банер" src="./img/common/banner.png"/>
                </div>
            </div>
           
        </section>
    )
}
export default Catalog;