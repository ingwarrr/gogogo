const events = [
    {
        title:'Limp Bizkit',
        id:0,
        brif:'Кажется, что яркий квартет поднялся на музыкальный олимп стремительно, но это не так. Тяжелая многолетняя работа...',
        category:'show',
        categoryId:1,
        rating:'16+',
        url:'limp-bizkit-spb-2019',
        categoryAlias : 'концерт',
        location:'СК «Юбилейный»',
        date:'24.09.2019',
        price:2200
        
    },
    {
        title:'As I Lay Dying',
        titleAlias:'аслы асалэйдаен asalaydying',
        id:1,
        brif:'Титаны американского металкора, законодатели стиля и кумиры целого поколения возвращаются...',
        category:'show',
        categoryId:1,
        rating:'16+',
        url:'as-i-lay-dying-kosmonavt-2019',
        categoryAlias:'концерт',
        location:'«Космонавт»',
        date:'24.09.2019',
        price:2500
    },
    {
        title:'Оно. Глава вторая',
        id:2,
        brif:'Проходит 27 лет после первой встречи ребят с демоническим Пеннивайзом. Они уже выросли...',
        category:'movie',
        categoryId:0,
        rating:'18+',
        url:'ono-glava-vtoraya',
        categoryAlias:'кино',
        location:'Кинотеатр «ПИК»',
        date:'24.09.2019',
        price:300
    }
    ,
    {
        title:'«Марио Тестино: Cуперзвезда» ',
        id:3,
        brif:'Марио Тестино — патриарх фэшн-фотографии, снимки которого появлялись на страницах Vogue, Vanity Fair и...',
        category:'exhibition',
        categoryId:3,
        rating:'18+',
        url:'mario-testino-superzvezda',
        categoryAlias:'выставка',
        location:'Музей современного искусства «Эрарта»',
        date:'31.05.2019',
        price:550
    }
    ,
    {
        title:'«Сад сновидений» ',
        id:4,
        brif:'Это выставка световых и интерактивных инсталляций, включающая в себя 15 зон — кошмар, осознанный сон...',
        category:'exhibition',
        categoryId:3,
        rating:'6+',
        url:'sad-snovidenii',
        categoryAlias:'выставка',
        location:'Креативное пространство «Люмьер-Холл»',
        date:'10.11.2019',
        price:400
    }
];
const cats = [
    {   
        id:0,
        title:'кино'
    },
    {   
        id:1,
        title:'концерты'
    },
    {   
        id:2,
        title:'театр'
    },
    {   
        id:3,
        title:'выставки'
    },
    {   
        id:4,
        title:'все'
    },
];
const onPageNav = [
    {
        title:'дешевле',
        id:0
    },
    {
        title:'дороже',
        id:1
    },
    {
        title:'по id',
        id:2
    }
];
export {onPageNav,cats,events}